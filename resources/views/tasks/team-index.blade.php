@extends('layouts.welcome')

@section('content')
           
<div class="my-3 my-md-5">
            <div class="container">
              <div class="page-header">
                <h1 class="page-title">Current Team Tasks</h1> 
                <div class="row gutters-xs ml-auto">
                    <div class="col">
                        <a href="{{route('task.create')}}" class="btn btn-success">Create Task</a>
                    </div>
                </div>
              </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-list-task" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 2.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-.5-.5H2zM3 3H2v1h1V3z"/>
                            <path d="M5 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM5.5 7a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 4a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9z"/>
                            <path fill-rule="evenodd" d="M1.5 7a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5V7zM2 7h1v1H2V7zm0 3.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5H2zm1 .5H2v1h1v-1z"/>
                        </svg>
                            <thead>
                                <th>Task Name</th>
                                
                                <th>From</th>
                                
                                <th>For</th>

                                <th>Category Name</th>

                                <th>Action</th>

                                <th>Status</th>

                             
                            
                            </thead>
                            <tbody>
                            @foreach ($tasks as $task)
                                    <tr>
                                        <td class="name"><div>{{ $task->name }}</div></td>
                                        <td class="name"><div>{{ $task->user->name }}</div></td>
                                        <td class="name"><div>{{ $task->to_user->name }}</div></td>
                                        @if(isset($task->category))
                                        <td class="name"><div>{{ $task->category->name }}</div></td>
                                        @else <td>NoCategory</td>
                                        @endif
                                        
                                        

                                        <!-- Task Delete Button -->
                                        <td>
                                            
                                            <form action="{{ route('task.delete', ['task' => $task->id, 'type' => 'team']) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                
                                                <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-danger">
                                                
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>

                                                <a href="{{route('task.edit',$task->id)}}" class="btn btn-info">Edit</a>
                                                <a href="{{route('task.show',$task->id)}}" class="btn btn-primary">Show Task </a>
                                            </form>
                                            @if($task->status=='pending')
                                            <form action="{{ route('task.make_completed', ['task' => $task->id, 'type' => 'team']) }}"method="POST">
                                                {{ csrf_field() }}
                                                <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-success">
                                                
                                                    <i></i>Mark Complete
                                                </button>
                                            
                                            </form>
                                            @else
                                            <form action="{{ route('task.make_pending', ['task' => $task->id, 'type' => 'team']) }}"method="POST">
                                                {{ csrf_field() }}
                                                <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-warning">
                                                
                                                    <i></i>Mark Pending
                                                    </button>
                                                    @endif
                                            
                                            </form>
                                           <td>@if($task->status=='pending')
                                <p class="text-danger text-light bg-dark">Pending</p>
                              @else
                              <p class="text-success text-light bg-dark">Done</p>
                              @endif
                                           </td>
                                        </td>
                                        
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            
        </div>
    </div>
@endsection
