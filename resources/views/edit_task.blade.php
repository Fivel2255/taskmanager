@extends('layouts.welcome')

@section('content')

<div class="container">
                <div class="page-header d-flex justify-content-center">
                <h2 class="page-title">Edit Task</h2> 
              </div>

                    <form action="{{ route('task.update', ['task' => $item->id]) }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Change Task Name</label>
                    

                            <div class="col-sm-6">
                            <textarea class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" placeholder="Task name...">{{old('name')}}</textarea>
                                    @if($errors->has('description'))
                                       <p class="text-danger">{{$errors->first('name')}}</p>
                                    @endif
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="task-name" class="col-sm-3 control-label">Description</label>
                    
                           
                            <div class="col-sm-6">
                                    <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" placeholder="Task description...">{{old('description')}}</textarea>
                                    @if($errors->has('description'))
                                       <p class="text-danger">{{$errors->first('description')}}</p>
                                    @endif
                                </div>   
                                </div>   
                                
                  

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">

                                <a href="/ToDoListGood/public/personal-tasks" class="btn btn-default btn-sm" role="button">Close</a>
                                <button type="submit" class="btn btn-success">
                                    <i class=""></i>Save
                                </button>

            
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        
    
@endsection
