<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
  
</head>
<body>


<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
      <a class="navbar-brand" href="/ToDoListGood/public/personal-tasks">ToDoListGood</a>
    
    
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/ToDoListGood/public/personal-tasks">Personal Tasks</a></li>
        <li class="active"><a href="/ToDoListGood/public/team-tasks">Team Tasks</a></li>
        <li class="active"><a href="/ToDoListGood/public/categories">Categories</a></li>
       
      </ul>
      </ul>


<ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                <li><a href="{{ url('personal-tasks') }}"><i class="fa fa-btn glyphicon glyphicon-home"></i>Profile</a></li>
            </ul>
        </li>
    @endif
</ul>
    </div>
  </div>
  
</nav>
@yield('content')

<!DOCTYPE html>
<html>
  <head>
    <title>Title of the document</title>
    <style>
      .header {
        height: 40px;
        padding: 20px 20px 0;
        background: #000000;
      }
      .main-content {
        height: 60vh;
        padding: 20px;
      }
      footer {
        padding: 10px 20px;
        background: #000000;
        position: absolute;
        color: white;
        bottom: 0;
        width: 100%;
      }
      a {
        color: #00aaff;
      }
    </style>
  </head>
  <body>
    <footer class="footer">
    <div class="container">
      <div class="align-items-center">
        <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
          Copyright &copy; 2020 <a href="https://www.instagram.com/ionut.folea/" class="text-info">Folea Virgil</a>. Developed by <a class="text-info" href="https://www.instagram.com/ionut.folea/">Folea Ioan Virgil</a>.All rights reserved.
        </div>
      </div>
    </div>
  </footer>
  </body>
</html>