@extends('layouts.welcome')

@section('content')


            <div class="container">
                <div class="page-header d-flex justify-content-center">
                <h2 class="page-title">Create New Task</h2> 
              </div>
                    <form action="{{ url('task') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Task</label>
                    

                            <div class="col-sm-6">
                                <input type="text"  name="name" id="task-name" class="form-control" value="{{ old('task') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Description</label>
                    
                            <div class="form-group">
                            <div class="col-sm-6">
                            <textarea class="form-control {{($errors->has('description'))?'is-invalid':''}}" name="description" placeholder="Task description...">{{old('description')}}</textarea>
                                    @if($errors->has('description'))
                                       <p class="text-danger">{{$errors->first('description')}}</p>
                                    @endif
                                </div>  
                                </div>   
                            
                        </div>
                        
                        <div class="form-group">
                            
                             <label for="person-todo" class="col-sm-3 control-label"></label>

                                 <div class="col-sm-6">
                                  <h4><span class="badge badge-info">From You</span></h4>

                            </div>
                            
                        </div>

                        <div class="form-group">
                    
                    
              
                            <label for="person-todo" class="col-sm-3 control-label">For</label>

                            <div class="col-sm-6">
                             <select name="for_user_id" id="">
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}"><div>{{ $user->name }}</div></option>
                                @endforeach
                             </select>

                            </div>
                        </div>
                        <div class="form-group">
              
                            <label for="person-todoDD" class="col-sm-3 control-label">Category Name</label>

                            <div class="col-sm-6">
                             <select name="task_category" id="">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"><div>{{ $category->name }}</div></option>
                                @endforeach
                             </select>

                            </div>
                        </div>
                     
                        
                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-plus"></i>Add Task
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
@endsection
