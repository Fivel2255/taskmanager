@extends('layouts.welcome')


@section('content')    

<div class="container">
                <div class="page-header d-flex justify-content-center">
                <h2 class="page-title">Create Category</h2> 
              </div>
              
                    <form action="{{ route('category.store2') }}" method="POST">
                        
                        
                        <div class="form-group">
                        
                            <label for="task-name" class="col-sm-3 control-label">Change Category Name</label>
                    
                            
                            <div class="col-sm-6">
                            <div class="form-group">
                            <textarea class="form-control {{($errors->has('name'))?'is-invalid':''}}" name="name" placeholder="Category name...">{{old('name')}}</textarea>
                                    @if($errors->has('name'))
                                       <p class="text-danger">{{$errors->first('name')}}</p>
                                    @endif
                            </div>
                        </div>

                        
                
                       

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">

                                <a href="/ToDoListGood/public/categories" class="btn btn-default btn-sm" role="button">Close</a>
                                <button type="submit" class="btn btn-success">
                                    <i class=""></i>Save
                                </button> 
                        </form>
                    </div>
                  </div>
                </div>
                        
              
            </div>            
          </div>
      </div>      
@endsection
