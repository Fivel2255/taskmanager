<?php

namespace App\Repositories;

use App\User;
use App\Categories;

class CatRepository
{
    /**
     * 
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return Categories::where('user_id', $user->id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
    public function all()
    {
        return Categories::orderBy('created_at', 'asc')
                    ->get();
    }
}
