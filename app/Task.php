<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'for_user_id', 'category_id', 'description'];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
        'name' => 'string',
        'for_user_id' => 'int',
        'category_id' => 'int',
        'description' => 'string',
    ];

    /**
     * Get the user that owns the task.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user that owns the task.
     */
    public function to_user()
    {
        return $this->belongsTo(User::class, 'for_user_id');
    }
    public function category(){
        return $this->belongsTo(Categories::class);
    }
}
