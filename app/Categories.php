<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;
use App\Projects;

class Categories extends Model{
    protected $fillable=['name'];

    public function tasks(){
        return $this->hasMany(Task::class);
    }

    
}
