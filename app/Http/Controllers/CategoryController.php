<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use Validator;
use App\Http\Controllers\Controller;
use App\Repositories\TaskRepository;



class CategoryController extends Controller
{
    /**
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_categories=Categories::all();
        return view('categories',['categories'=>$all_categories]);
    }

    /**
     * Create a new controller instance.
     *
     * @param  TaskRepository  $tasks
     * @return void
     */
    public function __construct(TaskRepository $tasks)
    {
        $this->middleware('auth');

        $this->tasks = $tasks;
       //$this->categories  = $categories ;
    }
    
    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add_category');
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store2(Request $request)
    {
        $validators=Validator::make($request->all(),[
            'name'=>'required|unique:categories',
        ]);
        if($validators->fails()){
            return redirect()->route('category.create')->withErrors($validators)->withInput();
        }else{
            $category=new Categories();
            $category->name=$request->name;
            $category->save();
            return redirect()->route('category.index')->with('message','Category created successfully !');
        }
    }

    

    /**
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit2($id)
    {
        $category=Categories::where('id',$id)->get();
        return view('edit_category',['category'=>$category]);
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update2(Request $request, $id)
    {
        
            $this->validate($request, [
                'name' => 'required|max:255',
                ]);
       
      
            $category=Categories::find($id);
            $category->name=$request->name;
            $category->save();
            return redirect()->route('category.index')->with('message','Category updated successfully !');

    }

    /**
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $find_category=Categories::find($id);
        $find_category->delete();
        return redirect()->route('category.index')->with('message','Category removed successfully !');
    }
}
