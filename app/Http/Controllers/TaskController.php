<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Categories;
use App\Task;
use App\User;
use App\Repositories\TaskRepository;
use App\Repositories\CatRepository;
class TaskController extends Controller
{
    /**
     * The task repository instance.
     *
     * @var TaskRepository
     */
    protected $tasks;

    /**
     * Create a new controller instance.
     *
     * @param  TaskRepository  $tasks
     * @return void
     */
    public function __construct(TaskRepository $tasks)
    {
        $this->middleware('auth');

        $this->tasks = $tasks;
       //$this->categories  = $categories ;
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  Request  $request
     * @param  Task  $task
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        

        $item = Task::find($id);
        $type = 'edit';
        return view('edit_task', compact('item', 'type'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_categories=Categories::all();
        $count_categories=Categories::count();
        return view('add_task',['categories'=>$all_categories,'count'=>$count_categories]);
    }


    /**
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Task  $task
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->authorize('destroy', $id);
        $this->validate($request, [
            'name' => 'required|max:255',
            'description'=>'required',

            ]);

        $input = $request->except(['_token', '_method']);
        $item = Task::find($id);
        $item->update($input);
        $item->save();
        return redirect('/personal-tasks');
    }

    /**
     * 
     *
     * @param  Request  $request
     * @return Response
     */
     public function personal(Request $request)
    {

        return view('tasks.index', [
            'tasks' => $this->tasks->forUser($request->user()),
            'users' => User::all(),
            'categories' => Categories::all(),
        ]);
           
    }
     /**
     * 
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {

        return view('create_task', [
            'tasks' => $this->tasks->forUser($request->user()),
            'users' => User::all(),
            'categories' => Categories::all(),
        ]);
    }
    /**
     * 
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'task_category'=>'required',
            'description'=>'required' 
            
        ]);
    
        $request->user()->tasks()->create([
            'name' => $request->name,
            'for_user_id' => $request->for_user_id,
            'category_id' => $request->task_category,
            'description'=>$request->description,
        ]);
        
        return redirect('/personal-tasks');
    }
    
    public function team(Request $request)
    {
        return view('tasks.team-index', [
            'tasks' => $this->tasks->all(),
            'users' => User::all()
            
        ]);
    }

    /**
     *
     *
     * @param  Request  $request
     * @param  Task  $task
     * @return Response
     */
    
    public function destroy(Request $request, Task $task)
    {
        //print_r($request->type);
        //die();
        $this->authorize('destroy', $task);

        $task->delete();
        if($request->type === "personal" ){

            return redirect('/personal-tasks');
        
        } 
        else {
            return redirect('/team-tasks');

        }
     
        }

        /**
     * @param  Task  $task
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $find_task=Task::where('id',$id)->get();
        $all_categories=Categories::all();
        return view('view_task',['tasks'=>$find_task,'categories'=>$all_categories]);
    }


     /**
     * 
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function makeCompleted(Request $request, $id)
    {
        $find_task = Task::find($id);
       // die('ssss');
        $find_task->status='completed';
        $find_task->save();
        if($request->type === "personal" ){

            return redirect('/personal-tasks');
        
        } 
        else {
            return redirect('/team-tasks');

        }
    }

    
    public function makePending(Request $request, $id){
        $find_task=Task::find($id);
        $find_task->status='pending';
        $find_task->save();
        if($request->type === "personal" ){

            return redirect('/personal-tasks');
        
        } 
        else {
            return redirect('/team-tasks');

        }
    }
}