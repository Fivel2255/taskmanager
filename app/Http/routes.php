<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    })->middleware('guest');

    Route::get('/personal-tasks', 'TaskController@personal');
    Route::get('/task/{task}', 'TaskController@edit')->name('task.edit');
    Route::get('/tasks/{task}', 'TaskController@show')->name('task.show');
    Route::post('/update-task/{task}', 'TaskController@update')->name('task.update');
    Route::get('/task', 'TaskController@index')->name('task.create');
    Route::get('/team-tasks', 'TaskController@team');
    Route::post('/task', 'TaskController@store');
    Route::delete('/delete-task/{task}/{type}', 'TaskController@destroy')->name('task.delete');
    Route::auth();
    Route::post('/personal-tasks/{id}/make_pending','TaskController@makePending')->name('task.make_pending');  
    Route::post('/personal-tasks/{id}/make_completed','TaskController@makeCompleted')->name('task.make_completed');  
    

});

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    })->middleware('guest');

    Route::get('/categories','CategoryController@index')->name('category.index');
    Route::get('/categories/create','CategoryController@create')->name('category.create');
    Route::post('/categories/store2','CategoryController@store2')->name('category.store2');
    Route::get('/categories/{id}/edit2','CategoryController@edit2')->name('category.edit2');
    Route::post('/categories/{id}/update2','CategoryController@update2')->name('category.update2');
    Route::delete('/categories/{id}/delete','CategoryController@destroy')->name('category.delete');
    Route::auth();
});
